1. What the application does
Our game is to control a ball on a plane and to collect as many stars as possible to get a higher score in a time period. You can press space on keyboard or touch screen on surface to make the ball jump. The ball can only stay on the plane, game will over when the time runs out or the ball falls off the plane.

2. How to use it (especially the user interface aspects)
UI: the main menu has a brief explanation about the rules of the game and the controls. There is also an option menu for adjusting the BGM volume.
Game mechanics: The accelerometer is used to control the direction of the player. Tap on the screen will make the player jump, tap rapidly will make the player jump higher.

3. How you modelled objects and entities. 
The stars on the ground is worth 1 point. There is a challenge star on the highest plane which is worth 20 points, but very hard to get.
Models: The model used in the game includes a ball, stars with different sizes, and several planes. Positions and movement of the stars are randomly generated.

4.  How you handled graphics and camera motion
Graphics :  A customised cel shader is used to the ball (the player), to produce a cartoon-style effect , the effect is changing through the day (e.g reflection disappears at night),a "shadow pass" is added to the cel shader so that the ball will cast a shadow. Gouraud shader is used to the star to perform colour change effect according to its self rotation. 
A particle system is used to boost the player up to a higher plane.
Other Shadows are implemented with the Unity build-in shadow effect, changing according to the direction light.
Camera motion: The camera will always focus on the player, but two fingers can be used to zoom in/out.

5. A statement about any code/APIs you have sourced/used from the internet that is not your own.
The implementation of the Cel shader is adapted from the online tutorial: https://www.youtube.com/watch?v=3qBDTh9zWrQ
Gouraud shader is adapted from the lab exercise solution.
Camera zooming is adapted from the lab exercise solution.

A video of how the game is player is here:
https://www.youtube.com/watch?v=cP_LlqHEP_Q

If the model of stars doesn't load,try to re-import the stars in the prefab folder and blender model folder 