﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public float jumpHeight;

    public Text score;
    public Text time;

	public Text restartText;
	public Text gameoverText;

	private bool gameover;
	private bool hasplayed=false;

	public AudioSource scoreSound;
	public AudioSource gameoverSound;

    public ParticleSystem lift;

    private Rigidbody rb;
    private int scoreCount;
    private float timeLeft;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        scoreCount = 0;
        timeLeft = 50;
		gameover = false;
		restartText.text = "";
		gameoverText.text = "";

    }

    void Update()
    {
        if (gameover)
        {
            Time.timeScale = 0;  //freeze the game
        }
		if (!gameover) 
		{
			Time.timeScale = 1;
		}
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
        if(WithinLiftRegion())
        {
            Lift();
        }
        
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(-moveVertical, 0.0f, moveHorizontal);

        rb.AddForce(movement * speed);
        if (!TestDropped())
        {
            timeLeft -= Time.deltaTime;
        }
        SetScoreAndTime();

        
    }

    void Jump()
    {
        rb.AddForce(jumpHeight * Vector3.up);
    }
    void Lift()
    {
        rb.AddForce(jumpHeight*0.3f * Vector3.up);
    }

    bool TestDropped()
    {
        if(transform.position.y<0.4)
        {
            timeLeft = 0;
            return true;
        }
        return false;
    }

    bool WithinLiftRegion()
    {
        if(Mathf.Abs((transform.position.x-lift.transform.position.x))<1&&
            Mathf.Abs((transform.position.z - lift.transform.position.z)) < 1&&
            transform.position.y<2)
        return true;

        return false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Star"))
        {
            other.gameObject.SetActive(false);
            scoreCount++;
			scoreSound.Play ();
        }
        else if(other.gameObject.CompareTag("BigStar"))
        {
            other.gameObject.SetActive(false);
            scoreCount+=25;
            scoreSound.Play();
        }
    }

    void SetScoreAndTime()
    {
        score.text = "Score: " + scoreCount.ToString();
        time.text = string.Format("Time Left: {0:0.00} secs",timeLeft);
        
        if(timeLeft<=0)
        {
            GameOver();

        }
    }
    // still needs to be completed
    void GameOver()
    {
		gameoverText.text = "Game Over!";
		gameover = true;
		restartText.text = "Restart";
		if (!hasplayed) 
		{
			gameoverSound.Play ();
			hasplayed = true;
		}

    }
		

  
}