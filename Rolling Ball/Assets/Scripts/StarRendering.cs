﻿using UnityEngine;
using System.Collections;

public class StarRendering : MonoBehaviour {

    public Shader shader;
    private GameObject player;
    public Color playerColor;

    void Start () {

        MeshRenderer renderer = this.gameObject.GetComponent<MeshRenderer>();
        renderer.material.shader = shader;

        MeshFilter cylinder = this.gameObject.GetComponent<MeshFilter>();
        Mesh m = cylinder.mesh;

        player = GameObject.Find("Player");

        //assgin the color map 
        int i;
        Color[] colors = new Color[m.vertices.Length];
        for(i=0;i<m.vertices.Length;i++)
        {
            colors[i] = Color.blue;
        }
        m.colors = colors;
    }

    // Update is called once per frame
    void Update () {
        // Get renderer component (in order to pass params to shader)
        MeshRenderer renderer = this.gameObject.GetComponent<MeshRenderer>();
       
        // Pass updated light positions to shader
        renderer.material.SetColor("_PointLightColor", playerColor);
        renderer.material.SetVector("_PointLightPosition", player.transform.position);
    }
}
