﻿using UnityEngine;
using System.Collections;

public class Star3Movement : MonoBehaviour {
	// moving forward and backward
	public float maxDistance = 2.5f;
	private float distance;
	private float moveSpeed;
	private float minX;
	private float maxX;
	private int direction = 1; // Switches when cube reverses direction

	void Start() {
		
		distance = Random.Range (0.25f, maxDistance);
		moveSpeed = Random.Range (0.5f, 1.5f);
		minX = transform.position.x - distance / 2;
		maxX = transform.position.x + distance / 2;

		// in this case there is no boundary
	}


	void Update () {

		this.transform.localPosition += Vector3.right * Time.deltaTime * moveSpeed * direction;
		if (this.transform.localPosition.x > maxX) {
			direction = -1;
		} else if (this.transform.localPosition.x < minX) {
			direction = 1;
		}

	}
}
