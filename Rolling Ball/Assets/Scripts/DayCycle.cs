﻿using UnityEngine;
using System.Collections;

public class DayCycle : MonoBehaviour {
    private Light sun;
    // Use this for initialization
    void Start () {
         sun= GetComponent<Light>();
    }
	
	// Update is called once per frame
	void Update() { 
        transform.Rotate(0.08f, 0, 0);
        //Debug.Log(transform.rotation.x);
        if(transform.rotation.x>0.96)
        {
            sun.enabled = false;
        }
        else if (transform.rotation.x < -0.2)
        {
            sun.enabled = true;
        }
    }
}
