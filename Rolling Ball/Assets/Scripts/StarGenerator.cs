﻿using UnityEngine;
using System.Collections;

public class StarGenerator : MonoBehaviour {

	public GameObject[] stars;
	public float starNumber;

	// the position limitation of generating the stars 
	private float minZ;
	private float maxZ;
	private float startX;
	public float endX;
	private float minHeight;
	public float maxHeight = 1.2f;


	// Use this for initialization
	void Start () {
		// the constraint of stars position
		minZ = GameObject.Find ("WallLeft").transform.position.z+0.1f;
		maxZ = GameObject.Find ("WallRight").transform.position.z-0.1f;
		startX = GameObject.Find ("Player").transform.position.x;
		minHeight = GameObject.Find ("Plane").transform.position.y+0.1f;

		StarSpawn ();

	}
	
	void StarSpawn() {
		// generating the stars
		for (int i = 0; i < starNumber; i++) {
			GameObject star = stars[Random.Range (0, stars.Length)];
			Vector3 starPosition = new Vector3 (Random.Range (startX, endX), 
				Random.Range (minHeight, maxHeight), Random.Range (minZ, maxZ));
			Quaternion starRotation = Quaternion.identity;
			Instantiate (star, starPosition, starRotation);
		}
			
	}


}
