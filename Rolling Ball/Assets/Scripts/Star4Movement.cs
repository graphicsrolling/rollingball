﻿using UnityEngine;
using System.Collections;

public class Star4Movement : MonoBehaviour {
	// moving up and down
	public float maxDistance = 0.5f;
	private float distance;
	private float moveSpeed;
	private float minY;
	private float maxY;
	private int direction = 1; // Switches when cube reverses direction

	void Start() {
		
		distance = Random.Range (0.2f, maxDistance);
		moveSpeed = Random.Range (0.2f, 1f);
		minY = transform.position.y;
		maxY = transform.position.y + distance * 2;


		// check the boundary
		if (minY < GameObject.Find ("Plane").transform.position.y) {
			minY = GameObject.Find ("Plane").transform.position.y;
		}


	}


	void Update () {

		this.transform.localPosition += Vector3.up * Time.deltaTime * moveSpeed * direction;
		if (this.transform.localPosition.y > maxY) {
			direction = -1;
		} else if (this.transform.localPosition.y < minY) {
			direction = 1;
		}

	}
}
