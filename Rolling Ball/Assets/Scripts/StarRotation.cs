﻿using UnityEngine;
using System.Collections;

public class StarRotation : MonoBehaviour {

	public float tumble = 5;

	void Start() {
		GetComponent<Rigidbody> ().angularVelocity = Random.insideUnitSphere * tumble;
	}

}