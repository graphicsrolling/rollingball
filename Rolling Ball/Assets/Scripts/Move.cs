﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Move : MonoBehaviour {

    public float speed;
    public float jumpHeight;

    private Rigidbody rb;


    private Dictionary<int, float> touchBeginTime;

    void Start()
    {
        this.touchBeginTime = new Dictionary<int, float>();
        rb = GetComponent<Rigidbody>();
    }


	void Update () {
        rb.AddForce(Input.acceleration.z * speed * Time.deltaTime, 0.0f, Input.acceleration.x * speed * Time.deltaTime);
        //this.transform.Translate(Input.acceleration.z * speed * Time.deltaTime, 0.0f, Input.acceleration.x * speed * Time.deltaTime);

        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch touch = Input.GetTouch(i);
            if (touch.phase == TouchPhase.Ended)
            {
                float touchBeginTime = -1.0f;
                this.touchBeginTime.TryGetValue(touch.fingerId, out touchBeginTime);
                if(touchBeginTime<0 || (Time.time - touchBeginTime) < 0.2f)
                {
                    rb.AddForce(jumpHeight * Vector3.up);
                }
                else
                {
                    this.touchBeginTime.Remove(touch.fingerId);
                }
            }
            else if(touch.phase == TouchPhase.Began)
            {
                this.touchBeginTime[touch.fingerId] = Time.time;
            }
        }
	}
}
