﻿using UnityEngine;
using System.Collections;

public class Star2Movement : MonoBehaviour {
	// moving left and right
	private float distance;
	private float moveSpeed;
	private float minZ;
	private float maxZ;
	private int direction = 1; // Switches when cube reverses direction

	void Start() {
		// calculate the max distance
		distance = (float)(GameObject.Find ("WallRight").transform.position.z - GameObject.Find ("WallLeft").transform.position.z);
		// random the distance between 0.1 and teh max distance
		distance = Random.Range (0.1f, distance);
		moveSpeed = Random.Range (0.1f, 1f);
		minZ = transform.position.z - distance / 2;
		maxZ = transform.position.z + distance / 2;


		// check the boundary
		if (minZ < GameObject.Find ("WallLeft").transform.position.z) {
			minZ = GameObject.Find ("WallLeft").transform.position.z;
		}
		if (maxZ > GameObject.Find ("WallRight").transform.position.z) {
			maxZ = GameObject.Find ("WallRight").transform.position.z;
		}
			
	}


	void Update () {
		
		this.transform.localPosition += Vector3.forward * Time.deltaTime * moveSpeed * direction;
		if (this.transform.localPosition.z > maxZ) {
			direction = -1;
		} else if (this.transform.localPosition.z < minZ) {
			direction = 1;
		}

	}
}
