﻿using UnityEngine;
using System.Collections;


public class CameraZooming : MonoBehaviour {

    public float zoomingSpeed = 0.2f;

    public static Vector3 cameraDistance;

    void Start()
    {
        cameraDistance = Vector3.zero;
    }

	void Update () {
	    if(Input.touchCount == 2)
        {
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);

            if(touch1.phase == TouchPhase.Moved && touch2.phase == TouchPhase.Moved)
            {
                // difference in previous locations using dalta positions
                Vector3 prevDist = (touch1.position - touch1.deltaPosition) - (touch2.position - touch2.deltaPosition); 
                // current distance between finger touches
                Vector3 currDist = touch1.position - touch2.position; 

                float deltaZoom = prevDist.magnitude - currDist.magnitude;
                cameraDistance += this.transform.forward * deltaZoom * zoomingSpeed;
            }
        }
	}
}
