﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
Shader "GouraudShader"
{
	Properties
	{
		_PointLightColor ("Point Light Color", Color) = (0, 0, 0)
		_PointLightPosition ("Point Light Position", Vector) = (0.0, 0.0, 0.0)
	}
	SubShader
	{
		Pass
		{
		Tags{
		"LightMode" = "ForwardBase"
		"RenderType" = "Opaque"
			}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			uniform float3 _PointLightColor;
			uniform float3 _PointLightPosition;

			struct vertIn
			{
				float4 vertex : POSITION;
				float4 normal : NORMAL;
				float4 color : COLOR;
			};

			struct vertOut
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
			};

			// Calculate the colour on the vertex 
			vertOut vert(vertIn v)
			{
				vertOut o;

				
				float4 worldVertex = mul(unity_ObjectToWorld, v.vertex);
				float3 worldNormal = normalize(mul(transpose((float3x3)unity_WorldToObject), v.normal.xyz));

				float Ka = 2;//set a higher intensity for Ambient illumination
				float3 amb = v.color.rgb * UNITY_LIGHTMODEL_AMBIENT.rgb * Ka;

				//calculate the diffuse component
				float fAtt = 1;
				float Kd = 0.5;
				float3 L = normalize(_PointLightPosition - worldVertex.xyz);
				float LdotN = dot(L, worldNormal.xyz);
				float3 dif = fAtt * _PointLightColor.rgb * Kd * v.color.rgb * saturate(LdotN);
				
				//calculate the specular component
				float Ks = 1;
				float specN = 1; //set a small SRE value so the reflection is noticible
				float3 V = normalize(_WorldSpaceCameraPos - worldVertex.xyz);
				float3 R = 2*worldNormal*(LdotN)-L;
				float3 spe = fAtt * _PointLightColor.rgb * Ks * pow(saturate(dot(V, R)), specN);

				o.color.rgb = amb.rgb + dif.rgb + spe.rgb;
				o.color.a = v.color.a;

				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);

				return o;
			}
			//Gouraud shader no need for frament calculation
			fixed4 frag(vertOut v) : SV_Target
			{
				return v.color;
			}
			ENDCG
		}
	}
}
