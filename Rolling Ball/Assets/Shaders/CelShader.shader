﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "CelShader" {
	//For adjusting the parameters 
	Properties{
		_Color("Diffuse Material Color", Color) = (1,1,1,1)
		_UnlitColor("Unlit Color", Color) = (0.5,0.5,0.5,1)
		_DiffuseThreshold("Lighting Threshold", Range(-1.1,1)) = 0.1
		_SpecColor("Specular Material Color", Color) = (1,1,1,1)
		_Shininess("Shininess", Range(0.5,1)) = 1
		_OutlineThickness("Outline Thickness", Range(0,1)) = 0.1

	}

		SubShader{

		//first pass for cel shading
		Pass{

		Tags{ 
		"LightMode" = "ForwardBase" 
		}

		CGPROGRAM

	#pragma vertex vert 
	#pragma fragment frag
		
		uniform float4 _Color;
		uniform float4 _UnlitColor;
		uniform float _DiffuseThreshold;
		uniform float4 _SpecColor;
		uniform float _Shininess;
		uniform float _OutlineThickness;

	struct vertexInput {

		//Variables retrieved from Unity
		float4 vertex : POSITION;
		float3 normal : NORMAL;
		float4 texcoord : TEXCOORD0;

	};

	struct vertexOutput {

		float4 pos : SV_POSITION;
		float3 normalDir : TEXCOORD1;
		float4 lightDir : TEXCOORD2;
		float3 viewDir : TEXCOORD3;
		float2 uv : TEXCOORD0;
	};

	vertexOutput vert(vertexInput input)
	{
		vertexOutput output;

		//Calculate normal,pos,and view angle for the mesh 
		output.normalDir = normalize(mul(float4(input.normal, 0.0), unity_WorldToObject).xyz);

		float4 posWorld = mul(unity_ObjectToWorld, input.vertex);

		output.viewDir = normalize(_WorldSpaceCameraPos.xyz - posWorld.xyz); 

																			
		float3 fragmentToLightSource = (_WorldSpaceCameraPos.xyz - posWorld.xyz);
		output.lightDir = float4(
			normalize(lerp(_WorldSpaceLightPos0.xyz , fragmentToLightSource, _WorldSpaceLightPos0.w)),
			lerp(1.0 , 1.0 / length(fragmentToLightSource), _WorldSpaceLightPos0.w)
			);

		//fragmentInput output;
		output.pos = mul(UNITY_MATRIX_MVP, input.vertex);

		//UV-Map
		output.uv = input.texcoord;

		return output;

	}

	float4 frag(vertexOutput input) : COLOR
	{

		float nDotL = saturate(dot(input.normalDir, input.lightDir.xyz));

	//Diffuse threshold calculation
	float diffuseCutoff = saturate((max(_DiffuseThreshold, nDotL) - _DiffuseThreshold) * 1000);

	//Specular threshold calculation
	float specularCutoff = saturate(max(_Shininess, dot(reflect(-input.lightDir.xyz, input.normalDir), input.viewDir)) - _Shininess) * 1000;

	//Calculate Outlines
	float outlineStrength = saturate((dot(input.normalDir, input.viewDir) - _OutlineThickness) * 1000);


	float3 ambientLight = (1 - diffuseCutoff) * _UnlitColor.xyz; 
	float3 diffuseReflection = (1 - specularCutoff) * _Color.xyz * diffuseCutoff;
	float3 specularReflection = _SpecColor.xyz * specularCutoff;

	float3 combinedLight = (ambientLight + diffuseReflection) * outlineStrength + specularReflection;

	return float4(combinedLight, 1.0); 


	}

		ENDCG

	}
		//second pass for shadow casting 
		Pass {

		Tags{
		"LightMode" = "ShadowCaster"
		}

		CGPROGRAM

	#pragma vertex vert 
	#pragma fragment frag

	struct vertexInput {

		//Variables retrieved from Unity
		float4 vertex : POSITION;
		float3 normal : NORMAL;
		float4 texcoord : TEXCOORD0;

	};

	struct vertexOutput {

		float4 pos : SV_POSITION;
		float3 normalDir : TEXCOORD1;
		float4 lightDir : TEXCOORD2;
		float3 viewDir : TEXCOORD3;
		float2 uv : TEXCOORD0;
	};

	vertexOutput vert(vertexInput input)
	{
		vertexOutput output;

		//Calculate normal,pos,and view angle for the mesh 
		output.normalDir = normalize(mul(float4(input.normal, 0.0), unity_WorldToObject).xyz);

		float4 posWorld = mul(unity_ObjectToWorld, input.vertex);

		output.viewDir = normalize(_WorldSpaceCameraPos.xyz - posWorld.xyz);

		float3 fragmentToLightSource = (_WorldSpaceCameraPos.xyz - posWorld.xyz);
		output.lightDir = float4(
			normalize(lerp(_WorldSpaceLightPos0.xyz , fragmentToLightSource, _WorldSpaceLightPos0.w)),
			lerp(1.0 , 1.0 / length(fragmentToLightSource), _WorldSpaceLightPos0.w)
			);

		//fragmentInput output;
		output.pos = mul(UNITY_MATRIX_MVP, input.vertex);

		//UV-Map
		output.uv = input.texcoord;

		return output;

	}
	//Do nothing on the colour since it's only for shadow casting
	float4 frag(vertexOutput input) : COLOR
	{
		return float4(0,0,0,0);
	}

		ENDCG

		}
	}

}